
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.security.Provider.Service;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Bunny0_
 */
public class Productservice {
    private static ArrayList<Product> ProductList = new ArrayList<>();

    //Create
    public static boolean addProduct(Product product) {
        ProductList.add(product);
        return true;
    }

    //Delete
    public static boolean delProduct(Product product) {
        ProductList.remove(product);
        return true;
    }
    public static boolean delete(int index){
       ProductList.remove(index);
       return true;
   }
    public static boolean clearAll(){
       ProductList.removeAll(ProductList);
       return true;
   }



    //Read
    public static ArrayList<Product> getProductList() {
        return ProductList;
    }

    //Update
    public static boolean updateProduct(int index, Product product) {
        ProductList.set(index, product);
        return true;
    }

    public static Product getProduct(int index) {
        return ProductList.get(index);
    }
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("Fluk.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(ProductList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(Productservice.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Productservice.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("Fluk.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            ProductList = (ArrayList<Product>)ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(Productservice.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Productservice.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Productservice.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

}
